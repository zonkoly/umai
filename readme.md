

## Installation

Post App require Ruby, Redis, and Mysql Database.

Install the dependencies  and start the server.

Postman collection can be found in the repo

```sh
cd umai
bundle install
ruby app/database/build_database.rb 
bundle exec rackup -p 3000
```

To run All tests

```sh
rspec specs/*/*
```


## License

MIT

