require 'rack/reloader'
require_relative 'application'

use Rack::Reloader
run -> (env) {
  APP.call(env)
}