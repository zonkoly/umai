$enviroment = 'test'
require './config/requires.rb'
require 'faker'
describe FeedbacksController do 
   before(:all) do 
      @controller = FeedbacksController.new()
         username = Faker::Name.unique.name.gsub("'","")
         feedbacked_username = Faker::Name.unique.name.gsub("'","")
         @user = User.user_with_keys(username: username)
         @feedbacked_user = User.user_with_keys(username: feedbacked_username )
         @post = Post.new({
            user_id: @feedbacked_user.id, 
            title: 'Title',
            content: 'Content',
            ip: '10.1.1.1'
         })
         @post.save
         @post.load_post
         @feedback_params = {
            username: @user.username, 
            comment: 'comment'
         }
   end 
   before (:each) do 
      Thread.current.reset_connection
   end
   context "Test Creating feedback" do 
       
       it "Feedback User" do 
         params = @feedback_params.merge({user_id: @feedbacked_user.id})
         result = @controller.create(params)
         response = result.first
         status = result.last
         expect(status).to eq(200)
         expect(response[:data].count).to eq 1
       end

       it "Feedback Post" do 
         params = @feedback_params.merge({post_id: @post.id, rating: 4})
         result = @controller.create(params)
         response = result.first
         status = result.last
         expect(status).to eq(200)
         expect(response[:data].count).to eq 2
       end

       it "Already Feedbacked user" do 
         params = @feedback_params.merge({user_id: @feedbacked_user.id})
         result = @controller.create(params)
         response = result.first
         status = result.last
         expect(status).to eq 422
         expect(response[:errors]).to eq "Can't Create feedback"
       end
       
       it "Already Feedbacked post" do 
         params = @feedback_params.merge({post_id: @post.id, rating: 4})
         result = @controller.create(params)
         response = result.first
         status = result.last
         expect(status).to eq 422
         expect(response[:errors]).to eq "Can't Create feedback"
       end
       it "Missing Comment" do 
         params = @feedback_params.merge({user_id: @user.id})
         params.delete(:comment)
         result = @controller.create(params)
         response = result.first
         status = result.last
         expect(status).to eq(422)
         
       end
    end
 end
 