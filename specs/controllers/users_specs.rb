$enviroment = 'test'
require './config/requires.rb'
require 'faker'
describe UsersController do 
    context "Test Creating User" do 
       before(:all) do 
        @username = Faker::Name.unique.name.gsub("'","")
       end
       it "Test Creating User" do 
           params = {
               username: @username
           }
           response = UsersController.new().create(params)
           expect(response[0][:data].nil?).to eq false
           expect(response[1]).to eq 200
           
       end

       it "Creating User with same name" do 
         params = {
            username: @username
        }
         response = UsersController.new().create(params)
         expect(response[0][:error]).to eq ["Username already exists"]
         expect(response[1]).to eq 422
      end
       
    end
 end
 