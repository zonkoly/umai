$enviroment = 'test'
require './config/requires.rb'
require 'faker'
describe PostsController do 
   before(:all) do 
      
      @params = {
         title: 'Title',
         content: 'Content',
         ip: '10.1.1.1',
         username: Faker::Name.unique.name.gsub("'","")
      }
   end 
   context "Test Creating post" do 
       
       it "Creating with existing User" do 
         username = Faker::Name.unique.name.gsub("'","")
         user = User.user_with_keys(username: username )
         params = @params.merge({username: username})
         result = PostsController.new().create(params)
         data = result.first
         status = result.last
         expect(data[:data].nil?).to eq false
         expect(status).to eq 200
       end

       it "Creating with non-existing user" do 
         username = Faker::Name.unique.name.gsub("'","")
         params = @params.merge({username: username})
         result = PostsController.new().create(params)

         data = result.first
         status = result.last
         expect(data[:data].nil?).to eq false
         expect(status).to eq 200
       end

       it "Creating with missing title" do 
         params = @params.dup
         params.delete(:title)
         result = PostsController.new().create(params)
         data = result.first
         status = result.last
         expect(data[:data].nil?).to eq true
         expect(status).to eq 422
       end
       
       it "Creating with missing content" do 
         params = @params.dup
         params.delete(:content)

         result = PostsController.new().create(params)
         data = result.first
         status = result.last
         expect(data[:data].nil?).to eq true
         expect(status).to eq 422
       end
       it "Creating with missing ip" do 
         params = @params.dup
         params.delete(:ip)

         result = PostsController.new().create(params)
         data = result.first
         status = result.last
         expect(data[:data].nil?).to eq true
         expect(status).to eq 422
       end
    end
 end
 