$enviroment = 'test'
require './config/requires.rb'
require 'faker'
describe RatingsController do 
    context "Test Creating post" do 
      before(:all) do
         return unless @post.nil?
         @controller = RatingsController.new()
         username = Faker::Name.unique.name.gsub("'","")
         @user = User.user_with_keys(username: username)
         @post = Post.new({
            user_id: @user.id, 
            title: 'Title',
            content: 'Content',
            ip: '10.1.1.1'
         })
         
         @post.save
         @rating_params = {
            username: @user.username, 
            post_id: @post.id, 
            rating: rand(1.0...5.0).round(2)
         }
      end
       it "Creating Multiple Ratings and compare results" do 
         ratings = Array.new(10) {rand(1.0...5.0)}.map{|r| r.round(1)}
         params = @rating_params
         ratings.each do |rating|
            params[:rating] = rating
            result = @controller.create(params)
            response = result.first
            status = result.last
            expect(status).to eq 200
            
         end

         @post.load_post
         expect(@post.rating).to eq (ratings.sum / ratings.count).round(2)
         expect(@post.rating_count).to eq (ratings.count)
         
       end

       it "Create Rating with non-existing user" do 
         username = Faker::Name.unique.name.gsub("'","")
         params = @rating_params
         params[:username] = username
         result = @controller.create(params)
         response = result.first
         status = result.last
         expect(status).to eq 200
       end

       it "Create rating for non-existing post" do 
         params = @rating_params
         params[:post_id] = @post.id + 1
         result = @controller.create(params)
         response = result.first
         status = result.last
         expect(status).to eq 422
       end
       
    end
 end
 