$enviroment = 'test'
require './config/requires.rb'

describe Post do 
    context "Test Creating post" do 
       before(:all) do 
        user = User.user_with_keys(username: 'username')

        @post_params = {
          title: 'Post Title', 
          content: 'Post Content', 
          user_id: user.id,
          ip: '10.10.10.1'

        }
       end
       it "Mandatory Fields" do 
         post = Post.new({})
         expect(post.save).to eq false 
         expect(post.errors).to eq ["Title required", "Content required", "Author required", "Author IP required" ]
       end

       it "Invalid User id" do 
        params = @post_params.dup
        params[:user_id] = 100
         post = Post.new(params)
         expect(post.save).to eq false
       end

       it "Valid Post" do 
        post = Post.new(@post_params)
        expect(post.save).to eq true
        end
    end
 end
 