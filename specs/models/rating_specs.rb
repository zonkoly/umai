$enviroment = 'test'
require './config/requires.rb'

describe Rating do 
   before(:each) do 
      @user = User.user_with_keys(username: 'username', id: nil) 
      @post = Post.new({
         title: 'Post Title', 
         content: 'Post Content', 
         user_id: @user.id,
         ip: '10.10.10.1'
      })
      @post.save
   end
    context "Test Creating Rate" do 
       
       it "Multiple Rate" do
         
         rate_params = {
            owner_id: @user.id, 
            post_id: @post.id, 
            rating: 3
         }
         
         expect(@post.rate_post(rate_params)).to eq true
         @post.load_post
         expect(@post.rating).to eq 3
         expect(@post.rating_count).to eq 1
         
         rate_params[:rating] = 5
         expect(@post.rate_post(rate_params)).to eq true
         @post.load_post
         expect(@post.rating).to eq (3 + 5).to_f / 2.to_f
         expect(@post.rating_count).to eq 2 
       end
       it "Invalid Rating value" do
         
         rate_params = {
            owner_id: @user.id, 
            post_id: @post.id, 
            rating: -1
         }
         
         expect(@post.rate_post(rate_params)).to eq false

         rate_params[:rating] = 6
         expect(@post.rate_post(rate_params)).to eq false
         
         rate_params[:rating] = nil 
         expect(@post.rate_post(rate_params)).to eq false
       end

       it "Concurrent Rates" do 
         
         rate_params = {
            owner_id: @user.id, 
            post_id: @post.id, 
            rating: 3
         }
         threads = []
         num_of_rates = 20
         num_of_rates.times do 
            threads << Thread.new do 
               expect(@post.rate_post(rate_params)).to eq true
            end
         end
         ThreadsWait.all_waits(*threads)
         @post.load_post
         expect(@post.rating).to eq 3
         expect(@post.rating_count).to eq num_of_rates
       end
    end
 end
 