$enviroment = 'test'
require './config/requires.rb'
require 'faker'
describe Feedback do 
   before(:all) do 
      @feedbacked = User.user_with_keys(username: 'feedbakced')
      @feedbacker = User.user_with_keys(username: 'feedbacker')
      @user_params = {
         user_id: @feedbacked.id, 
         owner_id: @feedbacker.id, 
         comment: Faker::Name.unique.name.gsub("'","")
      }

     @post = Post.new({
         title: 'Post Title', 
         content: 'Post Content', 
         user_id: @feedbacked.id,
         ip: '10.10.10.1'
     })
     @post.save
      @post_params = {
         post_id: @post.id, 
         rating: rand(1.0...5.0).round(2),
         owner_id: @feedbacker.id, 
         comment: Faker::Name.unique.name.gsub("'","")
      }
   end 
   context "User Feedbacks" do 
       
      
       it "Create Feedback" do 
         
         
         feedback = Feedback.new(@user_params)
         expect(feedback.save).to eq true

         Thread.current.reset_connection()
         feedback = Feedback.new(@user_params)
         feedback.save
       end
    end

    context "Post Feedbacks" do 
       

      it "Create Feedback" do 
        feedback = Feedback.new(@post_params)
        expect(feedback.save).to eq true

        Thread.current.reset_connection()
        feedback = Feedback.new(@post_params)
        feedback.save
      end
   end
 end
 