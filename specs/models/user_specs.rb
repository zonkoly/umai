$enviroment = 'test'
require './config/requires.rb'

describe User do 
    context "Test Creating User" do 
       
       it "Find User" do 

         params = {
            username: 'existing_username'
         }
         user = User.new(params)
         expect(user.create).to eq true
         result = User.user_with_keys(username: params[:username])
         expect(result.id).to eq user.id
       end

       it "Create User" do 
         params = {
            username: 'username'
         }  
         result = User.user_with_keys(username: 'username', id: nil)
         expect(result.username).to eq 'username'
       end

       it "Unique Username" do 
         params = {
            username: 'unique_username'
         }
         user = User.new(params)
         expect(user.create).to eq true

         user = User.new(params)
         expect(user.create).to eq false
         expect(user.errors).to eq ["Username already exists"]
       end

       it "Mandatory Username" do 
         user = User.new({})
         expect(user.create).to eq false 
         expect(user.errors).to eq ["Username required"]  
       end
    end
 end
 