# frozen_string_literal: true

require "rack/utils"
require "json"
require "./config/requires.rb"
#
# utility
#
@comp = ->(f1, f2) {
  ->(x) {
    f1[f2[x]]
  }
}
identity = ->(x) {
  x
}
@constant = ->(x) {
  ->(_) {
    x
  }
}
@second = ->((_a, b)) {
  b
}

#
# Middleware
#
query_to_params = ->(q) {
  Rack::Utils.parse_nested_query(q)
}
query_from_env = ->(env) {
  env["QUERY_STRING"]
}

env_to_params = @comp[query_to_params, query_from_env]

# Merge Inline & body params to env[:params]
env_with_params = ->(env) {
  body_string = env["rack.input"].read
  body_params = body_string.empty? ? {} : JSON.parse(body_string)
  env.merge(params: body_params.merge(env_to_params[env]).transform_keys(&:to_sym).to_json)
}

params_middleware = ->(handler) {
  @comp[handler, env_with_params]
}

# Add Content Type to the headers
content_type_middleware = ->(type) {
  ->(handler) {
    ->(env) {
      status,
      headers,
      body = handler[env]
      [status, headers.merge("Content-Type" => type), body]
    }
  }
}
json_content_type_middleware = content_type_middleware["text/json"]

# Convert Response from Controller to standard rake response [status, headers, body_array]
def convert_response(response) [response[1], {}, [response[0].to_json]] end

#
# Handlers
#
puts "REQUIRNG HANDLERS"
require "./handlers.rb"

#
# Routing
#
puts "REQUIRNG Routes"
require "./routes.rb"

#
# app
#
middleware_list = [
  params_middleware,
  json_content_type_middleware,
]
app_middleware = middleware_list.reduce(identity, &@comp)

APP = app_middleware[@router]
