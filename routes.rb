=begin
  Define Application Routes
=end

exact_path_matcher = ->(path, method) {
  ->(env) {
    env["PATH_INFO"] == path && env["REQUEST_METHOD"] == method
  }
}

routes = [
  [exact_path_matcher["/users/login", "POST"], @login_user_handler],
  [exact_path_matcher["/users", "POST"], @create_user_handler],
  [exact_path_matcher["/posts", "GET"], @top_posts_handler],
  [exact_path_matcher["/posts", "POST"], @create_posts_handler],
  [exact_path_matcher["/posts/authors", "GET"], @authors_handler],
  [exact_path_matcher["/rates", "POST"], @create_rating_handler],
  [exact_path_matcher["/feedbacks", "POST"], @create_feedback_handler],
  [@constant.call(true), @not_found_handler],
]

route_matcher = ->(env) {
  ->((cond, _handler)) {
    cond[env]
  }
}

find_route = ->(env) {
  routes.find(&route_matcher[env])
}

find_handler = @comp[@second, find_route]

@router = ->(env) {
  find_handler[env][env]
}
