class Feedback < BaseModel
  table_name = "feedbacks"
  attr_accessor :id, :user_id, :post_id, :owner_id, :comment, :owner_feedbacks

  def parse_hash(hash)
    hash = hash.transform_keys(&:to_sym)
    self.id = hash[:id]
    self.user_id = hash[:user_id]
    self.post_id = hash[:post_id]
    self.owner_id = hash[:owner_id]
    self.comment = hash[:cmt] || hash[:comment]
  end

  def validate
    self.errors = []
    self.errors << "Missing Comment" if self.comment.nil? || self.comment.empty?
    self.errors << "Missing Reviewed Member" if self.user_id.nil? && self.post_id.nil?
    return !self.errors.any?
  end

  def save
    return false unless validate

    feedback_params = {
      cmt: self.comment,
      owner_id: self.owner_id,
      user_id: self.user_id,
      post_id: self.post_id,

    }
    hash = FeedbackAdapter.new().create_feedback(feedback_params)

    unless hash.nil?
      self.owner_feedbacks = hash.map { |feedback_hash| Feedback.new(feedback_hash) }
      return true
    else
      self.errors = "Failed to Create Feedback"
      return false
    end
  end

  def as_json
    json = {
      id: self.id.to_i,
      comment: self.comment,
      owner_id: self.owner_id,
    }

    # Optional --> To Return all fields or based on feedback type (user/post)
    json[:user_id] = self.user_id unless self.user_id.nil?
    json[:post_id] = self.post_id unless self.post_id.nil?
    json
  end

  # Return All Feedbacks with post rating in case of post_feedbacks 
  def self.feedback_statistics
    FeedbackAdapter.new().feedback_statistics.map { |hash| hash.delete_if { |k, v| v.nil? } }
  end
end
