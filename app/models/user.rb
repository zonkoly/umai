class User < BaseModel
  table_name = "users"
  attr_accessor :id, :username

  def parse_hash(user_params)
    user_params = user_params.transform_keys(&:to_sym)
    self.id = user_params[:id]
    self.username = user_params[:username]
  end

  def validate_user()
    self.errors = []
    if self.username.nil? || self.username.empty?
      self.errors << "Username required"
    else
      self.errors << "Username already exists" unless UserAdapter.new().find_user(username: username).first.nil?
    end
    return errors.empty?
  end

  def create
    user_params = {
      username: self.username,
    }
    unless validate_user()
      return false
    end
    hash = UserAdapter.new().create_user(user_params)
    parse_hash(hash)
    return true
  end

  # Find or Create User with username or id
  def self.user_with_keys(username: nil, id: nil)
    user_params = UserAdapter.new().find_user(username: username, id: id).first
    return User.new(user_params) unless user_params.nil?

    user = User.new({ username: username })
    if user.create
      return user
    else
      return nil
    end
  end

  def as_json
    {
      id: self.id,
      username: self.username,
    }
  end
end
