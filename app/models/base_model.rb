class BaseModel
  attr_accessor :errors

  def initialize(hash)
    self.errors = ""
    parse_hash(hash)
  end

  def parse_hash(hash)
  end
end
