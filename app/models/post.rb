$RATING_MUTEX = Mutex.new

class Post < BaseModel
  table_name = "posts"

  attr_accessor :id, :title, :content, :user_id, :ip, :username, :rating, :rating_count

  def parse_hash(hash)
    hash = hash.transform_keys(&:to_sym)
    self.id = hash[:id]
    self.title = hash[:title]
    self.content = hash[:content]
    self.user_id = hash[:user_id]
    self.username ||= hash[:username]
    self.ip = hash[:ip]
    self.rating = hash[:rating] || 0
    self.rating_count = hash[:rating_count] || 0
    self.rating = self.rating.to_f.round(2)
    self.rating_count = self.rating_count.to_i
  end

  # Reload post from database
  def load_post()
    hash = PostAdapter.new().find_post(self.id)
    if hash.first.nil?
      return false
    else
      parse_hash(hash.first)
      return true
    end
  end

  def validate_post()
    self.errors = []
    self.errors << "Title required" if self.title.nil? || self.title.empty?
    self.errors << "Content required" if self.content.nil? || self.content.empty?
    self.errors << "Author required" if self.user_id.nil?
    self.errors << "Author IP required" if self.ip.nil? || self.ip.empty?
    self.errors.empty?
  end

  def save()
    self.user_id ||= user()&.id
    
    unless validate_post()
      return false
    end
    post_hash = as_json(); post_hash.delete(:id)


    if self.id.nil?
      begin
        hash = PostAdapter.new().create_post(post_hash)
      rescue
        self.errors = ["Validation Errors"]
        return false
      end
      unless hash.nil?
        parse_hash(hash)
        return true
      else
        return false
      end
    else
      PostAdapter.new().update_post(post_hash.slice(:id, :rating, :rating_count), self.id)
      return true
    end
  end

  def user
    user = User.user_with_keys(username: self.username, id: self.user_id)
    return user
  end

  def rate_post(rating_params)
    # Lock Mutex to avoid writing conflicts
    rate = Rating.new(rating_params)
    unless rate.validate
      self.errors = rate.errors
      return false
    end
    $RATING_MUTEX.synchronize {
      self.rating_count += 1
      self.rating = RatingAdapter.new().rate_post(rating_params).round(2)
    }
    return true
  end

  
  def self.top_posts(page: 1, per_page: 10)
    post_jsons = PostAdapter.new().find_top_posts(page: page, per_page: per_page)
    return post_jsons.map { |post_json| Post.new(post_json) }
  end

  def as_json()
    {
          id: self.id.to_i,
          username: self.username,
          rating: (self.rating.to_f),
          rating_count: self.rating_count.to_i,
          title: self.title,
          content: self.content,
          ip: self.ip,
          user_id: self.user_id,
        }
  end

  # Return all author_ips, for each return all usernames using it as an array
  def self.author_statistics(authors)
    PostAdapter.new().author_statistics(authors).map { |record|
      {
              ip: record["ip"],
              authors: record["authors"].split(","),
      }
    }
  end
end
