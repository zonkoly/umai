class Rating < BaseModel
  table_name = "ratings"
  attr_accessor :id, :rating, :post_id, :owner_id, :post_id

  def parse_hash(hash)
    hash = hash.transform_keys(&:to_sym)
    self.id = hash[:id]
    self.rating = hash[:rating].to_f
    self.post_id = hash[:post_id]
    self.owner_id = hash[:owner_id]
    self.post_id = hash[:post_id] || hash[:post_id]
  end

  def validate()
    self.errors = []
    self.errors << "Rating Required" if self.rating.nil? 
    self.errors << "Rating must be between 1-5" if self.rating < 1 || self.rating > 5 
    return self.errors.empty?
  end
  def save
    return false  unless validate()
    rating_hash = {
      rating: self.rating,
      user_id: self.owner_id,
      post_id: self.post_id,

    }
    hash = RatingAdapter.new().create_rating(rating_hash)

    unless hash.nil?
      parse_hash(hash.first)
      return true
    else
      self.errors = "Unkown Error"
      return false
    end
  end

  def as_json
    {
          id: self.id.to_i,
          rating: self.rating.to_f,
          post_id: self.post_id.to_i,
          user_id: self.owner_id
        }
  end
end
