def create_user_table(conn)
  # Create Table
  query = "CREATE TABLE users (
        id int NOT NULL AUTO_INCREMENT,
        username varchar(45) NOT NULL,
        PRIMARY KEY (id),
        UNIQUE KEY username_UNIQUE (username)
      ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;"
  conn.exec(query)

  # Procedure to create user and return user id
  conn.exec("CREATE PROCEDURE create_users(IN name VARCHAR(45), OUT last_id int)
                BEGIN
                INSERT INTO users (username) values (name);
                set last_id := last_insert_id();
                END")
end
