require "mysql2"

def create_database()
  client = Mysql2::Client.new(host: Settings.instance.value_for("database.host"), username: Settings.instance.value_for("database.username"), password: Settings.instance.value_for("database.password"))
  client.query("CREATE DATABASE #{Settings.instance.value_for("database.name")};")
  client.query("USE #{Settings.instance.value_for("database.name")};")
  client.close()
end
