def create_rating_table(conn)

  # Create Table
  query = "CREATE TABLE ratings (
        id int NOT NULL AUTO_INCREMENT,
        post_id int NOT NULL,
        rating float NOT NULL,
        comment mediumtext,
        user_id int NOT NULL,
        PRIMARY KEY (id),
        KEY feedbacker_id (user_id),
        KEY rating_post_id_idx (post_id),
        CONSTRAINT rating_post_id FOREIGN KEY (post_id) REFERENCES posts (id),
        CONSTRAINT rating_user_id FOREIGN KEY (user_id) REFERENCES users (id)
      ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;"
  conn.exec(query)

  # Procedure to rate post and update current post rating & rating count
  rate_post_procedure = "CREATE  PROCEDURE rate_post(IN post_id int, IN nrating float, IN user_id int, OUT post_rating float)
    BEGIN
	INSERT INTO ratings (post_id, user_id, rating) values (post_id, user_id, nrating);
    set post_rating := (select AVG(rating) from ratings where ratings.post_id = post_id);
    UPDATE posts 
		set rating = post_rating, rating_count = rating_count + 1
        where posts.id = post_id;
    END"

  conn.exec(rate_post_procedure)
end
