def create_posts_table(conn)

  # Create Table
  query = "CREATE TABLE posts (
        id int NOT NULL AUTO_INCREMENT,
        rating float NOT NULL DEFAULT '0',
        rating_count int NOT NULL DEFAULT '0',
        user_id int NOT NULL,
        username varchar(45) NOT NULL,
        title varchar(45) NOT NULL,
        content longtext NOT NULL,
        ip varchar(15) NOT NULL,
        PRIMARY KEY (id),
        KEY Rating (rating) USING BTREE,
        KEY user_id (user_id),
        CONSTRAINT user_id FOREIGN KEY (user_id) REFERENCES users (id)
      ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
      "

  conn.exec(query)

  # Procedure to Create Post and return newly inserted record id 
  conn.exec("CREATE PROCEDURE create_posts(IN ntitle text, IN ncontent text, IN nip varchar(15), IN nusername text, IN nrating float, IN nrating_count int, IN nuser_id int, OUT last_id int )
                BEGIN
                INSERT INTO posts (title, content, ip, rating, rating_count, user_id, username) values (ntitle, ncontent, nip, nrating, nrating_count, nuser_id, nusername);
	            set last_id := last_insert_id();
                END")
end
