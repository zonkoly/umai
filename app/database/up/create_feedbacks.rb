def create_feedback_table(conn)

  # Create Table 
  query = "CREATE TABLE feedbacks (
        id int NOT NULL AUTO_INCREMENT,
        user_id int,
        owner_id int NOT NULL,
        post_id int,
        cmt mediumtext NOT NULL,
        PRIMARY KEY (id),
        KEY feedback_user_id_idx (user_id),
        KEY feedback_post_id_idx (post_id),
        KEY feedback_owner_id_idx (owner_id),
        CONSTRAINT feedback_owner_id FOREIGN KEY (owner_id) REFERENCES users (id),
        CONSTRAINT feedback_user_id FOREIGN KEY (user_id) REFERENCES users (id),
        CONSTRAINT feedback_post_id FOREIGN KEY (post_id) REFERENCES posts (id)
      ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
      "
  conn.exec(query)

  # Procedure to feedback user / post, it first makes sure that the user didn't feedback (user/post) before, and return all feedbacks made by this user if success
  create_feedback_procedure = "
    CREATE PROCEDURE feedback_user_post(IN in_user_id int, IN in_post_id int , IN in_owner_id int, IN in_cmt text)
    BEGIN
        DECLARE feedback_count DECIMAL;
    SELECT 
        COUNT(*)
    FROM
        feedbacks
    WHERE
        (post_id = in_post_id
            OR user_id = in_user_id)
            AND owner_id = in_owner_id INTO feedback_count;
        IF feedback_count = 0 THEN
            INSERT INTO feedbacks (user_id, post_id, owner_id, cmt) values (in_user_id, in_post_id, in_owner_id, in_cmt);
        SELECT 
        *
    FROM
        feedbacks
    WHERE
        owner_id = in_owner_id;
        END IF; 
    END;"
  conn.exec(create_feedback_procedure)
end
