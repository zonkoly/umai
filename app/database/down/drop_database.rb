require "mysql2"

def drop_database()
  # Drop Database 
  client = Mysql2::Client.new(host: Settings.instance.value_for("database.host"), username: Settings.instance.value_for("database.username"), password: Settings.instance.value_for("database.password"))
  client.query("DROP DATABASE #{Settings.instance.value_for("database.name")};")
  client.close()
end
