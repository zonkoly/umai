=begin
  Responsible For Building Database
    - Drop existing one if exist
    - Create the new database 
    - Run migrations
=end 
require "./config/requires.rb"
["./app/database/up", "./app/database/down", "./app/database/adapters"].each do |dir|
  Dir["#{dir}/*.rb"].each { |file|
    require file
  }
end

puts "BUILDING DATABASE"

begin
  drop_database()
rescue Exception => e
end

begin
  create_database()
rescue Exception => e
end
conn = Connection.new()
# create tables

begin
  create_user_table(conn)

  create_posts_table(conn)

  create_rating_table(conn)

  create_feedback_table(conn)
ensure
  conn.close()
end
