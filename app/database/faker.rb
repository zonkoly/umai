require "faker"
require "ipaddr"
require "./config/requires.rb"
=begin 
  Seed database 
=end
class DatabaseFacker
  def fake_database(post_count: 200e3, users_count: 1ß00, ips_count: 50, user_feedbacks: 50e3, posts_feedbacks: 10e3, ratings_count: 10e3)
    begin
      Thread.current.reset_connection # Start database connection if not exist

      # Seed Users
      users = []
      users_count.times do
        username = Faker::Name.unique.name.gsub("'", "")
        user = User.new({ username: username })
        users << user if user.create
      end

      # Seed ip addresses
      ips = []
      ips_count.times do
        ips << IPAddr.new(rand(2 ** 32), Socket::AF_INET)
      end

      # Seed Posts
      posts = []
      post_count.to_i.times do
        user = users.sample
        hash = {
          username: user.username,
          user_id: user.id,
          ip: ips.sample(1)[0].to_s,
          title: Faker::Name.name.gsub("'", ""),
          content: Faker::Lorem.paragraphs,
        }
        post = Post.new(hash)
        posts << post if post.save
      end

      # Seed ratings
      ratings_count.to_i.times do
        user = users.sample
        post = posts.sample
        rate_params = {
          owner_id: user.id,
          post_id: post.id,
          rating: rand(1.0...5.0).round(2),
          username: user.username,
        }
        post.rate_post(rate_params)
      end

      # Seed User Feedbacks
      user_feedbacks.to_i.times do
        Thread.current.reset_connection
        feedback_users = users.sample(2)
        feedback_params = {
          user_id: feedback_users[0].id,
          owner_id: feedback_users[1].id,
          comment: Faker::Name.unique.name.gsub("'", ""),
        }
        feedback = Feedback.new(feedback_params)
        feedback.save
      end

      # Seed Posts Feedbacks
      posts_feedbacks.to_i.times do
        Thread.current.reset_connection
        user = users.sample
        feedback_params = {
          post_id: posts.sample.id,
          owner_id: user.id,
          comment: Faker::Name.unique.name.gsub("'", ""),
        }
        feedback = Feedback.new(feedback_params)
        feedback.save
      end
    ensure
      Thread.current.connection&.close() # Close Database Connection
    end
  end
end

DatabaseFacker.new().fake_database()
