class BaseAdapter
  # Adapters Used as a layer between Application and Database to be able to switch to any database type needed.
  attr_accessor :conn

  def initialize
    self.conn = Thread.current.get_connection
  end

  def convert_params(params, table_name)
    result = params.keys.map { |key|
      ["#{key}", "'#{params[key]}'"]
    }
    return result
  end

  def insert_query(params, table_name)
    "INSERT INTO #{table_name} (#{params.map { |param| param[0] }.join(", ")}) values (#{params.map { |param| param[1] }.join(", ")});"
  end
end
