require "./app/database/adapters/base_adapter.rb"

class RatingAdapter < BaseAdapter
  def create_rating(rating_params)
    params = convert_params(rating_params, "ratings")
    @conn.exec(insert_query(params, "ratings"))
  end

  def rate_post(rating_params)
    @conn.exec("CALL rate_post(#{rating_params[:post_id]}, #{rating_params[:rating]}, #{rating_params[:owner_id]}, @post_rating);")
    @conn.exec("select @post_rating").first["@post_rating"]
  end
end
