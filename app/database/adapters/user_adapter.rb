require "./app/database/adapters/base_adapter.rb"

class UserAdapter < BaseAdapter
  def create_user(user_params)
    params = convert_params(user_params, "users")

    @conn.exec("CALL create_users('#{user_params[:username]}', @user_id);")
    {
          id: @conn.exec("select @user_id").first["@user_id"],
          username: user_params[:username],
        }
  end

  def find_user(username: nil, id: nil)
    if username.nil?
      key = "id"
      value = id
    else
      key = "username"
      value = username
    end
    @conn.exec("SELECT * from users where users.#{key}='#{value}' LIMIT 1;")
  end
end
