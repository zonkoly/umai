require "mysql2"
require "./config/requires.rb"
# Custom Connection To Isolate Database Type 
class Connection
  attr_accessor :conn, :time

  def initialize()
    set_connetion()
  end

  def set_connetion
    self.conn = Mysql2::Client.new(host: Settings.instance.value_for("database.host"), username: Settings.instance.value_for("database.username"), password: Settings.instance.value_for("database.password"))
    self.conn.query("USE #{Settings.instance.value_for("database.name")}")
  end

  def exec(command)
    self.conn.query(command)
  end

  def close
    self.conn.close()
  end

  def reset
    close()
    set_connetion()
  end
end
