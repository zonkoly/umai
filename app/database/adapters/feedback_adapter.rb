require "./app/database/adapters/base_adapter.rb"

class FeedbackAdapter < BaseAdapter
  def create_feedback(feedback_params)
    query = "CALL feedback_user_post(#{feedback_params[:user_id] || "NULL"}, #{feedback_params[:post_id] || "NULL"}, #{feedback_params[:owner_id]}, '#{feedback_params[:cmt]}')"
    result = @conn.exec(query)
    @conn.reset()
    return result
  end

  def feedback_statistics
    @conn.exec("
            SELECT 
                feedbacks.id,
                feedbacks.user_id,
                feedbacks.owner_id,
                feedbacks.post_id,
                feedbacks.cmt AS comment,
                posts.rating AS post_rating
            FROM feedbacks
            LEFT OUTER JOIN
                posts ON feedbacks.post_id = posts.id;
            ")
  end
end
