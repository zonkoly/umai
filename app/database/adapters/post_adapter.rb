require "./app/database/adapters/base_adapter.rb"
# require 'mysql2'
class PostAdapter < BaseAdapter
  def create_post(post_params)
    string_parameters = ["title", "content", "ip", "username"].map { |key| "'#{post_params[key.to_sym]}'" }.join(", ")
    number_parameters = ["rating", "rating_count", "user_id"].map { |key| post_params[key.to_sym] }.join(", ")
    post_attributes = [string_parameters, number_parameters].join(", ")

    @conn.exec("CALL create_posts(#{post_attributes}, @post_id);")

    return post_params.merge({
             id: @conn.exec("select @post_id").first["@post_id"],
           })
  end

  def find_post(post_id)
    @conn.exec("SELECT * from posts where id = #{post_id} LIMIT 1;")
  end

  def find_top_posts(page: 1, per_page: 10)
    offset = (page.to_i - 1) * per_page.to_i
    @conn.exec("
            select * from umai_develop.posts order by rating desc limit #{offset},#{per_page};").to_a
  end

  def update_post(post_params, id)
    params = convert_params(post_params, "posts")
    @conn.exec("UPDATE posts SET #{post_params.map { |key, val| "#{key} = #{val}" }.join(",")} where id = #{id}")
  end

  def author_statistics(authors)
    puts "SQL" 
    puts "SELECT ip, GROUP_CONCAT(DISTINCT(username)) as authors from posts where user_id in (SELECT id from users where username in (#{authors.map{|r| "'#{r}'"}.join(',')})) group by ip ;"
    @conn.exec("SELECT ip, GROUP_CONCAT(DISTINCT(username)) as authors from posts where user_id in (SELECT id from users where username in (#{authors.map{|r| "'#{r}'"}.join(',')})) group by ip ;")
  end
end
