require "redis"
require "json"

class PostsController

  def author_statistics(params)
    return [{ data: Post.author_statistics(params[:authors] || []) }, 200]
  end

  def create(params)
    
    param_keys = [:username, :ip, :title, :content]
    params = params.slice(:username, :ip, :title, :content)
    unless params.keys.size == param_keys.size
      return [
               {
                 error: "Missing Parameters #{params.keys} #{param_keys}",
               }, 422,
             ]
    end
    # Find or create user
    user = User.user_with_keys(username: params[:username])
    if user.nil?
      user = User.new(params)
      user.create
    end

    post = Post.new(params.merge({ user_id: user.id }))
    if post.save
      return [{
               data: post.as_json(),
             }, 200]
    else
      return [{
               error: post.errors,
             }, 422]
    end
  end

  def index(params)
    # Use Redis to Cache Posts for (5 * 60) seconds
    redis = Redis.new
    key = "top_posts_#{params[:page]}_#{params[:per_page]}"
    # Fetch Cached value for the key
    @result = redis.get(key)
    if @result.nil?
      # query database
      posts = Post.top_posts(page: params[:page], per_page: params[:per_page])
      @result = {
        data: posts.map { |post| post.as_json() },
      }.to_h

      redis.set(key, @result.to_json, ex: 5 * 60)
    else
      # parse cached value
      @result = JSON.parse(@result)
    end
    return [@result, 200]
  end
end
