class UsersController

  def create(params)
    user = User.new(params)
    if user.create
      return[{
              data: user.as_json(),
            }, 200]
    else
      return [
               {
                 error: user.errors,

               }, 422,
             ]
    end
  end

  def login(params)
    user = User.user_with_keys(username: params[:username])
    if user.nil?
      return [
               { error: "Not Found" }, 422,
             ]
    else
      return [{ data: user.as_json() }, 200]
    end
  end
end
