class RatingsController
  def create(params)
    params[:owner_id] = User.user_with_keys(username: params[:username]).id
    post = Post.new({ id: params[:post_id] })
    
    unless post.load_post()
      return [{ errors: "Not Found" }, 422]
    end
    if post.rate_post(params)

      return [{ data: post.as_json }, 200]
    else
      return [
        {errors: post.errors}, 
        422
      ]
    end
  end
end
