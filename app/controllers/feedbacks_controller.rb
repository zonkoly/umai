class FeedbacksController
  def create(params)
    params[:owner_id] = User.user_with_keys(username: params[:username]).id
    feedback = Feedback.new(params)
    unless feedback.save()
      return [
               { errors: "Can't Create feedback" },
               422,
             ]
    end

    return [
             {
               data: feedback.owner_feedbacks.map { |obj| obj.as_json },
             },
             200,
           ]
  end

  def feedback_statistics(params)
    return [{ data: Feedback.feedback_statistics }, 200]
  end
end
