require "yaml"
require "singleton"
=begin
  Application Settings/Enviroment Variables, stored in settings.yml for each enviroment
=end 
class Settings
  include Singleton
  attr_accessor :settings
  def self._load(str)
    instance.load_settings
    instance
  end

  def load_settings
    @settings = YAML.load_file("./settings.yml")[$enviroment]
  end

  # value for key, keys are '.' separated
  # ex: 'database.name'
  def value_for(key)
    load_settings() if @settings.nil?

    dict = @settings
    key.split(".").each do |subkey|
      dict = dict[subkey]
      break if dict.nil?
    end
    return dict
  end
end
