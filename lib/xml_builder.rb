require 'nokogiri'

def process_array(label,array,xml)
  array.each do |hash|
    xml.send(label) do                 
      hash.each do |key,value|
        if value.is_a?(Array)
          process_array(key,value,xml) 
        else
          xml.send(key,value)          
        end
      end
    end
  end
end

def build_xml_for_array(data, root_node)
    builder = Nokogiri::XML::Builder.new do |xml|
        xml.root do                           # Wrap everything in one element.
            process_array(root_node,data,xml)  # Start the recursion with a custom name.
        end
    end
    return builder.to_xml
end


