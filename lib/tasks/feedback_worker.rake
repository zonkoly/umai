require './config/requires.rb'
require './lib/xml_builder.rb'

namespace :feedback do
    desc "Generate XML file for Feedbacks"
    task :export  do

        Thread.current.get_connection
        feedbacks = Feedback.feedback_statistics()
        xml_string = build_xml_for_array(feedbacks, 'feedbacks')
        File.open("./xmls/feedback_#{Time.now.strftime("%d_%m_%Y_%H_%M")}.xml", 'w'){ |file|
            file.write(xml_string)
        }
    end
  
    
  end
  