=begin
    Initialize Enviroment variable and Load Settings from settings.yml
=end

$enviroment ||= 'production'


Settings.instance.load_settings

# Build Test Database
if $enviroment == 'test'
    require './app/database/build_database.rb'
end


Thread.class_eval do 
    require './config/requires.rb'
    attr_accessor :connection
    
    def get_connection
        if self.connection.nil?
            self.connection = Connection.new()
        end
        self.connection
    end

    # Reset Thread Databse Connection
    def reset_connection
        self.connection&.close()
        self.connection = nil
        self.get_connection
    end
end