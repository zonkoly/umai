require 'socket'
require 'json'
require './settings.rb'
require 'date'
require 'thread'
require 'thwait'

# Load Application Files
["./app/database/adapters", "./app/controllers", "./app/models"].each do |dir|
    Dir["#{dir}/*.rb"].each {|file|
        require file
        }
end

require './config/initialize.rb'