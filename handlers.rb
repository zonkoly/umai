
@create_user_handler = ->(env) {
  convert_response UsersController.new().create(JSON.parse(env[:params]).transform_keys(&:to_sym))
}
@login_user_handler = ->(env) {
  convert_response UsersController.new().login(JSON.parse(env[:params]).transform_keys(&:to_sym))
}

@top_posts_handler = ->(env) {
  convert_response PostsController.new().index(JSON.parse(env[:params]).transform_keys(&:to_sym))
}
@create_posts_handler = ->(env) {
  convert_response PostsController.new().create(JSON.parse(env[:params]).transform_keys(&:to_sym))
}
@authors_handler = ->(env) {
  convert_response PostsController.new().author_statistics(JSON.parse(env[:params]).transform_keys(&:to_sym))
}

@create_rating_handler = ->(env) {
  convert_response RatingsController.new().create(JSON.parse(env[:params]).transform_keys(&:to_sym))
}

@create_feedback_handler = ->(env) {
  response = FeedbacksController.new().create(JSON.parse(env[:params]).transform_keys(&:to_sym))
  convert_response(response)
}

@feedback_statistics_handler = ->(env) {
  convert_response FeedbacksController.new().feedback_statistics(JSON.parse(env[:params]).transform_keys(&:to_sym))
}

@not_found_handler = ->(env) {
  [404, {},
      [{
       errors: "404 Not found",
     }.to_json]]
}
